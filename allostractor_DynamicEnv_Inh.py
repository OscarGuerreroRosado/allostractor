from attractor import Attractor
import matplotlib.pyplot as plt
import numpy as np
import random
import math
import csv

temperature = True
thirst = True

dynamicQ = True
inh_condition = True

plotting = False
Group_simulations=True


num_timesteps = 100000 #100.000
num_simulations=50

simulation = 0

arena_size = 200
arena_limit = 6

thetaList = []
theta = 90

nav_noise = 10
wheel_w = 15

arenaX = [0,arena_size]
arenaY = [0,arena_size]
grid_size=1

dV_temperature = 1
dV_thirst = 1

att_normal_factor = 40

aVtemperature_list=[]
aVthirst_list=[]

Drive_temperature_list=[]
Drive_thirst_list=[]

TFtemperature_list=[]
TFthirst_list=[]

Iatt_temperature_list=[]
Iatt_thirst_list=[]

attractorQ_list = []

meanGrad_Temp = []


plast_cnt_temp = 0
plast_cnt_thirst = 0


if plotting == True:
    plt.ion()
    plt.style.use('seaborn')
    fig1, ax1 = plt.subplots(1, 2,figsize=(8,4))
    fig2, ax2 = plt.subplots(2, 2,figsize=(16,5))
    fig2.tight_layout(pad=2.0)




class Allostractor():

    def __init__(self):
        self.attractor = Attractor()
        self.robot_x = [random.randint(10,190)]
        self.robot_y = [random.randint(10,190)]

        self.gradient_position_float = 0

        self.create_temp_gradient(1)
        self.create_thirst_gradient()
        self.build_gradients()

        self.aVhomeo_temperature = 1
        self.aVhomeo_thirst = 1

        self.inhibition_counter = 0
        self.inhibition = False
        self.decision_temp = False
        self.decision_thirst = False
        
########################### GRADIENTS FUNCTIONS ###########################
    def kde_quartic(self,d,h):
        dn=d/h
        P=(15/16)*(1-dn**2)**2
        return P

    def sigmoid(self, x, timestep):
        limit_xInter = 0.55
        increment_xInter = (limit_xInter*2)/num_timesteps
        slope = 15

        xInter = -limit_xInter + increment_xInter*timestep
        return 1 / (1 + math.exp(-slope * (x-xInter)))



    def create_temp_gradient(self, currentTstep):
        global meanGrad_Temp

        arena_axis = np.arange(-1 , 1, 0.01)
        arena_axis = arena_axis[::-1]

        arena_column = []
        for i in range(arena_size):
            arena_column.append(self.sigmoid(arena_axis[i],currentTstep))

        for i in range(len(arena_column)):
            if arena_column[i]>0.98:
                arena_column[i] = 0.98

        temp_gradient = np.tile(arena_column, (200,1))
        temp_gradient = temp_gradient.transpose()

        #CONSTRUCT GRID
        x_grid=np.arange(arenaX[0],arenaX[1],grid_size)
        y_grid=np.arange(arenaY[0],arenaY[1],grid_size)
        self.x_mesh_temperature,self.y_mesh_temperature=np.meshgrid(x_grid,y_grid)
        self.temperature_intensity = temp_gradient

        meanGrad_Temp.append(self.temperature_intensity.mean())



    def create_thirst_gradient(self):
        self.thirst_intensity_list=[]

        #POINT DATASET
        x= [15]
        y= [185]
        #DEFINE GRID SIZE AND RADIUS(h)
        grid_size=1
        h=280
        #CONSTRUCT GRID
        x_grid=np.arange(arenaX[0],arenaX[1],grid_size)
        y_grid=np.arange(arenaY[0],arenaY[1],grid_size)
        self.x_mesh_thirst,self.y_mesh_thirst=np.meshgrid(x_grid,y_grid)
        #GRID CENTER POINT
        xc=self.x_mesh_thirst+(grid_size/2)
        yc=self.y_mesh_thirst+(grid_size/2)
        #PROCESSING
        for j in range(len(xc)):
            intensity_row=[]
            for k in range(len(xc[0])):
                kde_value_list=[]
                for i in range(len(x)):
                    #CALCULATE DISTANCE
                    d=math.sqrt((xc[j][k]-x[i])**2+(yc[j][k]-y[i])**2) 
                    if d<=h:
                        p=self.kde_quartic(d,h)
                    else:
                        p=0
                    kde_value_list.append(p)
                #SUM ALL INTENSITY VALUE
                p_total=sum(kde_value_list)
                intensity_row.append(p_total)
            self.thirst_intensity_list.append(intensity_row)


    def build_gradients(self):

        if thirst == True:
            thirst_min = 100
            thirst_max = 0
            for i in range(len(self.thirst_intensity_list)):
                for j in range(len(self.thirst_intensity_list[i])):
                    if self.thirst_intensity_list[i][j] < thirst_min:
                        thirst_min = self.thirst_intensity_list[i][j]
                    if self.thirst_intensity_list[i][j] > thirst_max:
                        thirst_max = self.thirst_intensity_list[i][j]

            thirst_intensity=np.array(self.thirst_intensity_list)
            self.thirst_intensity=thirst_intensity/thirst_max


    def plot_gradient(self):
        #...........  GRADIENTS ...........
        if temperature==True:
            ax1[0].cla()
            ax1[0].grid(False)
            ax1[0].plot(self.robot_x,self.robot_y,'g', markersize=3)
            ax1[0].set_title("Temperature")
            ax1[0].pcolormesh(self.x_mesh_temperature,self.y_mesh_temperature,self.temperature_intensity, cmap = plt.get_cmap('Blues_r'))


        if thirst==True:
            ax1[1].cla()
            ax1[1].grid(False)
            ax1[1].set_title("Thirst")
            ax1[1].plot(self.robot_x[-1],self.robot_y[-1],'ro')
            ax1[1].pcolormesh(self.x_mesh_thirst,self.y_mesh_thirst,self.thirst_intensity, cmap = plt.get_cmap('viridis'))


        fig1.canvas.flush_events()

        #...........  ATTRACTOR DYNAMICS ...........
        ax2[0,0].cla()
        ax2[0,0].grid(False)
        ax2[0,0].set_title("Mean Firing Rate")
        ax2[0,0].set_ylim(-0.1, 1.2)
        ax2[0,0].plot(TFtemperature_list, color='orange', label='Temperature')
        ax2[0,0].plot(TFthirst_list, color='blue', label='Thirst')
        ax2[0,0].legend(loc="upper left")

        ax2[1,0].cla()
        ax2[1,0].grid(False)
        ax2[1,0].set_ylim(-0.1, 1.1)
        ax2[1,0].set_title("Attractor Inputs")
        ax2[1,0].plot(Iatt_temperature_list, color='orange', label='Temperature')
        ax2[1,0].plot(Iatt_thirst_list, color='blue', label='Thirst')


        #...........  Q  ...........

        ax2[0,1].cla()
        ax2[0,1].set_title("Q value")
        ax2[0,1].set_ylim(-0.1, 1.1)
        ax2[0,1].plot(attractorQ_list, color='red')


        fig2.canvas.flush_events()




########################### LOCAL VIEWS ###########################

    def temperature_LV(self): #Local View
        self.q0_temperature, self.q1_temperature, self.q2_temperature, self.q3_temperature = 0,0,0,0

        for i in range(4):
            for j in range(3):
                self.q0_temperature += self.temperature_intensity[int(self.robot_y[-1]) + (j + 1), int(self.robot_x[-1]) - (i + 1)]
                self.q1_temperature += self.temperature_intensity[int(self.robot_y[-1]) + (j + 1), int(self.robot_x[-1]) + (i + 1)]
                self.q2_temperature += self.temperature_intensity[int(self.robot_y[-1]) - (j + 1), int(self.robot_x[-1]) - (i + 1)]
                self.q3_temperature += self.temperature_intensity[int(self.robot_y[-1]) - (j + 1), int(self.robot_x[-1]) + (i + 1)]

        self.q0_temperature /= 12
        self.q1_temperature /= 12
        self.q2_temperature /= 12
        self.q3_temperature /= 12
        
        
        self.aV_temperature = (self.q0_temperature + self.q1_temperature + self.q2_temperature + self.q3_temperature) / 4
        self.diff_temperature = abs(dV_temperature - self.aV_temperature)


    def thirst_LV(self):
        self.q0_thirst, self.q1_thirst, self.q2_thirst, self.q3_thirst = 0,0,0,0
        for i in range(4):
            for j in range(3):
                self.q0_thirst += self.thirst_intensity[int(self.robot_y[-1]) + (j + 1), int(self.robot_x[-1]) - (i + 1)]
                self.q1_thirst += self.thirst_intensity[int(self.robot_y[-1]) + (j + 1), int(self.robot_x[-1]) + (i + 1)]
                self.q2_thirst += self.thirst_intensity[int(self.robot_y[-1]) - (j + 1), int(self.robot_x[-1]) - (i + 1)]
                self.q3_thirst += self.thirst_intensity[int(self.robot_y[-1]) - (j + 1), int(self.robot_x[-1]) + (i + 1)]

        self.q0_thirst /= 12
        self.q1_thirst /= 12
        self.q2_thirst /= 12
        self.q3_thirst /= 12

        
        self.aV_thirst = (self.q0_thirst + self.q1_thirst + self.q2_thirst + self.q3_thirst) / 4
        self.diff_thirst = abs(dV_thirst - self.aV_thirst)




########################### ORIENTATION ###########################

    def adsign(self):
        global dV_temperature, dV_thirst
        self.adsign_temperature = np.sign(dV_temperature - self.aV_temperature)
        self.adsign_thirst = np.sign(dV_thirst - self.aV_thirst)

    def hsign(self):
        if theta <= 112 and theta > 77: #UP
            self.hsign_temperature = np.sign(self.q1_temperature - self.q0_temperature)
            self.hsign_thirst = np.sign(self.q1_thirst - self.q0_thirst)
        elif theta <= 157 and theta > 112: #UP-L
            self.hsign_temperature = np.sign(((self.q0_temperature + self.q1_temperature)/2) - ((self.q0_temperature + self.q2_temperature)/2))
            self.hsign_thirst = np.sign(((self.q0_thirst + self.q1_thirst)/2) - ((self.q0_thirst + self.q2_thirst)/2))
        elif theta <= 202 and theta > 157: #L
            self.hsign_temperature = np.sign(self.q0_temperature - self.q2_temperature)
            self.hsign_thirst = np.sign(self.q0_thirst - self.q2_thirst)
        elif theta <= 247 and theta > 202: #DOWN-L
            self.hsign_temperature = np.sign(((self.q2_temperature + self.q0_temperature)/2) - ((self.q2_temperature + self.q3_temperature)/2))
            self.hsign_thirst = np.sign(((self.q2_thirst + self.q0_thirst)/2) - ((self.q2_thirst + self.q3_thirst)/2))
        elif theta <= 292 and theta > 247: #DOWN
            self.hsign_temperature = np.sign(self.q2_temperature - self.q3_temperature)
            self.hsign_thirst = np.sign(self.q2_thirst - self.q3_thirst)
        elif theta <= 337 and theta > 292: #DOWN-R
            self.hsign_temperature = np.sign(((self.q3_temperature + self.q2_temperature)/2) - ((self.q3_temperature + self.q1_temperature)/2))
            self.hsign_thirst = np.sign(((self.q3_thirst + self.q2_thirst)/2) - ((self.q3_thirst + self.q1_thirst)/2))
        elif theta <= 22 and theta > 337: #R
            self.hsign_temperature = np.sign(self.q3_temperature - self.q1_temperature)
            self.hsign_thirst = np.sign(self.q3_thirst - self.q1_thirst)
        elif theta <= 77 and theta > 22: #UP-R
            self.hsign_temperature = np.sign(((self.q1_temperature + self.q3_temperature)/2) - ((self.q1_temperature + self.q0_temperature)/2))
            self.hsign_thirst = np.sign(((self.q1_thirst + self.q3_thirst)/2) - ((self.q1_thirst + self.q0_thirst)/2))



########################### HOMEOSTASIS ###########################

    def homeostasis(self):

        discount = 0.0005 #0.001
        bonus = discount*2.5

        self.aVhomeo_temperature -= discount
        self.aVhomeo_thirst -= discount

        if self.aVhomeo_temperature < 0: self.aVhomeo_temperature = 0
        if self.aVhomeo_thirst < 0: self.aVhomeo_thirst = 0

        
        if self.diff_temperature<0.2:
            self.aVhomeo_temperature += bonus

        if self.diff_thirst<0.05:
            self.aVhomeo_thirst += bonus

        if self.aVhomeo_temperature > 1: self.aVhomeo_temperature = 1
        if self.aVhomeo_thirst > 1: self.aVhomeo_thirst = 1

        if inh_condition == True:
            if ((dV_temperature - self.aVhomeo_temperature < 0.001 and dV_temperature - self.aVhomeo_temperature > -0.001) and self.decision_temp == True) or ((dV_thirst - self.aVhomeo_thirst < 0.001 and dV_thirst - self.aVhomeo_thirst > -0.001) and self.decision_thirst == True):
                #print("STARTING INHIBITION")
                self.inhibition = True
                self.decision_temp = False
                self.decision_thirst = False


            if self.inhibition == False:
                #The attractor input will be 1 - the Actual value, resulting in 0 when the system is satisfied
                self.Itemp_attractor = 1 - self.aVhomeo_temperature
                self.Ithi_attractor = 1 - self.aVhomeo_thirst
            else:
                self.Itemp_attractor = -40
                self.Ithi_attractor = -40
                self.inhibition_counter +=1
                self.decision_temp = False
                self.decision_thirst = False

                #print('INHIBITING cnt = ' + str(self.inhibition_counter))
                if self.inhibition_counter > 50:
                    self.inhibition = False
                    self.inhibition_counter = 0

        else:
            self.Itemp_attractor = 1 - self.aVhomeo_temperature
            self.Ithi_attractor = 1 - self.aVhomeo_thirst


        


        aVthirst_list.append(self.aVhomeo_thirst)
        aVtemperature_list.append(self.aVhomeo_temperature)

        Iatt_temperature_list.append(self.Itemp_attractor)
        Iatt_thirst_list.append(self.Ithi_attractor)

        #For Mutual information analysis Attractor input must be considered instead of Urgency measure.
        Drive_thirst_list.append(self.Ithi_attractor)
        Drive_temperature_list.append(self.Itemp_attractor)



########################### ATTRACTOR DYNAMICS ###########################

    def stress(self):

        global dynamicQ
        if dynamicQ == True:

            dVs = [dV_temperature, dV_thirst]
            aVs = [self.aVhomeo_temperature, self.aVhomeo_thirst]
            mse = 0.25 + np.mean((np.array(dVs)-np.array(aVs))**2)

            self.attractor.q = mse

        attractorQ_list.append(self.attractor.q)


    def attractor_dynamics(self):
        global att_normal_factor

        self.Itemp_attractor *= 10
        self.Ithi_attractor *= 10
        self.total_force_temperature, self.total_force_thirst = self.attractor.advance(self.Itemp_attractor, self.Ithi_attractor)

        if self.total_force_temperature > att_normal_factor or self.total_force_thirst > att_normal_factor:
            att_normal_factor = max(self.total_force_temperature, self.total_force_thirst)


        self.total_force_temperature /= att_normal_factor
        self.total_force_thirst /= att_normal_factor

        TFtemperature_list.append(self.total_force_temperature)
        TFthirst_list.append(self.total_force_thirst)

        #if self.total_force_temperature > 0.5 and self.total_force_thirst < 0.35 and self.inhibition == False:
        if self.total_force_temperature - self.total_force_thirst > 0.3 and self.inhibition == False:
            self.decision_temp = True
        else:
            self.decision_temp = False

        #if self.total_force_thirst > 0.5 and self.total_force_temperature < 0.35 and self.inhibition == False:
        if self.total_force_thirst - self.total_force_temperature > 0.3 and self.inhibition == False:
            self.decision_thirst = True
        else:
            self.decision_thirst = False



########################### NAVIGATION ###########################

    def conv(self, ang):
        x = np.cos(np.radians(ang)) 
        y = np.sin(np.radians(ang)) 
        return x , y

    def wheel_turning(self):
        self.wheel = -1 * ((self.hsign_temperature * self.adsign_temperature* self.total_force_temperature) + (self.hsign_thirst * self.adsign_thirst* self.total_force_thirst)) * (1/2)

    def random_navigation(self):
        global theta

        theta_extra = 5

        if theta > 360:
            div = math.trunc(theta/360) #num of rounds
            div *= 360                  #round in degrees
            theta = theta%div            #new theta
        if  theta < 0 and theta >= -360:
            theta +=360


        if(self.robot_x[-1]<arena_limit and self.robot_y[-1]<arena_limit): #Left-bottom
            theta = np.random.randint(20,70)
        elif(self.robot_x[-1]<arena_limit and self.robot_y[-1]>arena_size-arena_limit): #Left-top
            theta = np.random.randint(290,340)
        elif(self.robot_x[-1]>arena_size-arena_limit and self.robot_y[-1]<arena_limit): #Right-bottom
            theta = np.random.randint(110, 160)
        elif(self.robot_x[-1]>arena_size-arena_limit and self.robot_y[-1]>arena_size-arena_limit): #Right-top
            theta = np.random.randint(200,250)
        elif( self.robot_x[-1]<arena_limit ): #Left
            if theta <=180:
                theta -= theta_extra
            else:
                theta += theta_extra
        elif(self.robot_x[-1]>arena_size-arena_limit ): #Right
            if theta <=180 and theta >= 0:
                theta += theta_extra
            elif theta <= 0:
                theta = 0
            else:
                theta -= theta_extra
        elif(self.robot_y[-1]<arena_limit): #Bottom
            if theta >= 90 and theta <=270:
                theta -= theta_extra
            else:
                theta += theta_extra
        elif(self.robot_y[-1]>arena_size-arena_limit): #Top
            if theta >= 90 and theta <=270:
                theta += theta_extra
            else:
                theta -=theta_extra
        else:
            theta = theta + random.gauss(0, nav_noise) + self.wheel*wheel_w

    
        check_x = self.robot_x[-1]+self.conv(theta)[0] + np.random.uniform(-0.5,0.5)
        check_y = self.robot_y[-1]+self.conv(theta)[1] + np.random.uniform(-0.5,0.5)
        if check_x >= 4 and check_x <= arena_size - 4 and check_y >= 4 and check_y <= arena_size - 4:
            self.robot_x.append(check_x)
            self.robot_y.append(check_y)
        else:
            self.robot_x.append(self.robot_x[-1])
            self.robot_y.append(self.robot_y[-1])



########################### SAVING DATA ###########################
        
    def save_data(self, current_simulation):
        global dynamicQ, aVtemperature_list, aVthirst_list, Drive_temperature_list, Drive_thirst_list, TFtemperature_list, TFthirst_list, attractorQ_list, meanGrad_Temp

        if dynamicQ == True:
            if inh_condition == True:
                csv_namefile = '/home/roboticslab/Robotology/Repos/allostractor/data/dynamicEnv/dynamicQ_Inh/' + str(current_simulation+1) + '.csv'
            else:
                csv_namefile = '/home/roboticslab/Robotology/Repos/allostractor/data/dynamicEnv/dynamicQ_NoInh/' + str(current_simulation+1) + '.csv'
        else:
            if inh_condition == True:
                csv_namefile = '/home/roboticslab/Robotology/Repos/allostractor/data/dynamicEnv/staticQ_Inh/' + str(current_simulation+1) + '.csv'
            else:
                csv_namefile = '/home/roboticslab/Robotology/Repos/allostractor/data/dynamicEnv/staticQ_NoInh/' + str(current_simulation+1) + '.csv'
        print(csv_namefile)
        
        with open(csv_namefile, mode='w') as csv_file:
            csv_writer = csv.DictWriter(csv_file, fieldnames=['Xposition', 'Yposition', 'aVtemperature', 'aVthirst', 'DriveTemp', 'DriveThirst', 'TFtemperature', 'TFthirst', 'attractorQ', 'Grad_Temp'])
            csv_writer.writeheader()
            for i in range(num_timesteps):
                csv_writer.writerow({'Xposition': self.robot_x[i], 'Yposition': self.robot_y[i], 'aVtemperature': aVtemperature_list[i], 'aVthirst': aVthirst_list[i],
                    'DriveTemp': Drive_temperature_list[i], 'DriveThirst': Drive_thirst_list[i], 'TFtemperature': TFtemperature_list[i], 'TFthirst': TFthirst_list[i],
                    'attractorQ': attractorQ_list[i], 'Grad_Temp': meanGrad_Temp[i]})

    def clean(self):
        global aVtemperature_list, aVthirst_list, Drive_temperature_list, Drive_thirst_list, TFtemperature_list, TFthirst_list, attractorQ_list, Iatt_temperature_list, Iatt_thirst_list
        self.robot_x = [random.randint(10,190)]
        self.robot_y = [random.randint(10,190)]
        self.gradient_position_float = 0

        self.aVhomeo_temperature = 1
        self.aVhomeo_thirst = 1

        self.attractor.q = 0.5
        self.attractor.we1 = 5
        self.attractor.we2 = 5

        aVtemperature_list=[]
        aVthirst_list=[]

        Drive_temperature_list=[]
        Drive_thirst_list=[]

        TFtemperature_list=[]
        TFthirst_list=[]

        Iatt_temperature_list=[]
        Iatt_thirst_list=[]

        attractorQ_list = []
        



########################### RUN SIMULATION ###########################
    def run(self, current_simulation):
    
        for i in range(num_timesteps):
            self.create_temp_gradient(i+1)
            self.temperature_LV()
            self.thirst_LV()
            self.adsign()
            self.hsign()
            self.homeostasis()
            self.stress()
            self.attractor_dynamics()
            self.wheel_turning()
            self.random_navigation()
            if plotting == True:
                self.plot_gradient()
        self.save_data(current_simulation)
        self.clean()


###########################  ###########################

allo = Allostractor()

if __name__ == '__main__':
    try:
        if Group_simulations == True:
            for a in range(num_simulations):
                allo.run(a)
        else:
            allo.run(0)


    except KeyboardInterrupt:
        print('Simulation interrupted')