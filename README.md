# Allostractor

This repository is under development and constains different models of allsotasis to be published:

- [x] `2021Replication2G.py` - Replication of Sanchez-Fibla et al. (2010) 2 Gradients
- [x] `2021Replication3G.py` - Replication of Sanchez-Fibla et al. (2010) 3 Gradients
- [x] `allostractor_StaticEnv.py` - Drive-competition model in static environment (Security-Arousal)
- [x] `allostractor_DynamicEnv.py` - Drive-competition model in dynamic environment (Water-Temperature) - 
Dynamic/Fixed PV/SST ratio and Plasticity can be activated

An additional script allowed us to perform a parameter search previous to implementation
- [x] `q-var.py` - Combinations of PV/SST ratio and variability values.

Each script has a function of data storage (`save_data()`) where the variable `csv_namefile` must be modified according to the selected path where data will be stored.

`analysis` folder contains jupyter notebooks dedicated to analysing the data drawn from its related model version. CSV path file must be updated here again.
